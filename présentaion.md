
# Projet Redmine 
### Mise en situation POE Devops

Anne - Charles - David - Margaux 


---

## Qui sommes nous?

- Anne
    - blabla
- Charles
    - baba
- David
    - blabla
- Margaux
    - blabla
---
# Notre projet


- L'entreprise machin souhaite mettre en place redmine pour sa gestion de projet.
 [Here is how](https://mark.show/5eeb54a4381f5) 
---
# Les outils utilisés par l'équipe 
- Git-lab 
    - -> versionner et partager nos versions de code
- vagrant
    - -> deployer les machines (test et production)
- ansible
    - -> configurer les machines
- docker
    - -> deployer le micro-service.

Travail en mode projet agile

---
## L'architecture mise en place
![Notre archi globale](https://gitlab.com/charleslgs/test-toto/-/raw/main/archi_global_grp1.png)


---

# More?

[Check advance example](https://mark.show/?source=https://mark.show/demo.md)
- [ ] test



