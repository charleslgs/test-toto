---
# Configurations
# black, white, league, beige, sky, simple, serif, night, moon, solarized
theme: sky
# cube, page, concave, zoom, linear, fade, none, default 
transition: slide
#  Syntax highlighting style https://highlightjs.org/static/demo/
highlight: solarized-dark
backgroundTransition: zoom
progress: true
controls: true
hideAddressBar: true

# Editor settings
editor:
    fontSize: 14
    theme: solarized_light
    # keybinding: vim
---
# Charles
---
## Mon parcours
- Ingénieur en mécanique et matériaux
- 5 ans d'expérience dans l'industrie
    - aéronautique : matériaux et procédés
    - étiquettes RFID : méthode et industrialisation
    - chauffage : certification et chef de projet innovation

---
### Architecture mise en place
- Utilisation de Vagrant, Ansible et Docker
- Collaboration et gestion des versions via GitLab
- Un projet GitLab avec 3 dépots (Vagrant, Ansible et Docker)

<img src="https://gitlab.com/charleslgs/test-toto/-/raw/main/2021-08-18_08h54_30.png" alt="dépots Git" width="800"/>

---
### Architecture mise en place

<img src="https://gitlab.com/charleslgs/test-toto/-/raw/main/archi_global_grp1.png" alt="schéma_d'archi" width="600"/>

---
## Playbook de déploiement docker
```yaml
- name: User management
    hosts: docker
    become: true
    gather_facts: yes
    tasks:
```
<details>
  <summary ALIGN=LEFT> 1. Paramètrage utilisateur</summary>

</details>
<details>
  <summary ALIGN=LEFT> 2. Installation des dépendances docker</summary>

</details>
<details>
  <summary ALIGN=LEFT>3. Echange de clé ssh utilisation de l'API gitlab et du module uri</summary>



</details>

<details>
  <summary ALIGN=LEFT>4. Récupération du projet git sur la machine docker</summary>


</details>
---

### Playbook de déploiement docker

<details>
  <summary ALIGN=LEFT> 1. Paramètrage utilisateur</summary>

```yaml
    - name: Création d'un user ansible
        ansible.builtin.user:
        name: ansible
        groups: "sudo"
        generate_ssh_key: true
        ssh_key_type: "ed25519"
        ssh_key_file:  "{{ssh_key_file}}"
        state: present

    - name: Ajout de la clé publique
        ansible.posix.authorized_key:
        user: ansible
        state: present
        key: "{{ lookup('file', '/home/stagiaire/.ssh/id_ed25519.pub') }}"

```
</details>
<details>
  <summary ALIGN=LEFT> 2. Installation des dépendances docker</summary>

```yaml
- name: Installation prérequis python
    ansible.builtin.apt:
    name: ['python', 'python3-pip','python-setuptools']

- name: Installation librairies docker et docker-compose
    ansible.builtin.pip:
    name: ['docker', 'docker-compose', 'python-gitlab']
    executable: pip3
```
</details>
<details>
  <summary ALIGN=LEFT>3. Echange de clé ssh utilisation de l'API gitlab et du module uri</summary>
- Prérequis : Génération d'un token pour dialoguer avec l'API gitlab

```yml{css, echo=FALSE}

    - name : API pour pousser une clé ssh publique vers gitlab
    ansible.builtin.uri:
        url: "https://gitlab.com/api/v4/user/keys"
        method: POST
        status_code: [201, 400]
        headers:
        private-token: "{{ gitlab_token }}"
        Content-Type: "application/json"
        body: >
        {  "title": "Clé_machine_docker", 
            "key": "{{ blueprod_key_file }}"  }
        body_format: json
```

```{css, echo=FALSE}
.scroll-50 {
  max-height: 50px;
  overflow-y: auto;
  background-color: inherit;
}
```

</details>

<details>
  <summary ALIGN=LEFT>4. Récupération du projet git sur la machine docker</summary>

```yml
    - name: Clonage du git
      ansible.builtin.git:
        repo: git@gitlab.com:blueteam4/docker-compose.git
        dest: "{{docker_compose_path}}"
        accept_hostkey: True
        key_file: "{{prv_key_file_path}}"
```

</details>

<details>
  <summary ALIGN=LEFT>5. Déploiement du Docker Compose </summary>

```yaml
    - name: Create and start services
      community.docker.docker_compose:
        project_src: "{{ docker_compose_path }}"
      register: output
      tags: always

```

</details>

---

### Difficultés rencontrées et points d'améliorations
- utilisation d'une API
- échange de clefs SSH
